
PROMPT  
PROMPT =======================================================
PROMPT Afficher les caractéristiques de l'avatar principal
PROMPT =======================================================

SELECT alias, date_inscription
FROM joueur;

PROMPT  
PROMPT =======================================================
PROMPT Afficher les caractéristiques de l'avatar principal
PROMPT =======================================================

SELECT c.nom, b.nom, a.date_obtention, a.niveau
FROM caracteristique_avatar a JOIN caracteristique b ON a.id_caracteristique = b.id
                            JOIN avatar c ON c.id = a.id 
                                    WHERE REGEXP_LIKE (c.nom, '*\*');
                                    
PROMPT  
PROMPT =======================================================
PROMPT Afficher la liste des joueurs qui possèdent plus d'un item
PROMPT =======================================================
                
SELECT j.alias, a.nom
FROM joueur j JOIN avatar a ON j.id = a.id_joueur 
                JOIN itemAvatar c ON c.id_avatar = a.id
                GROUP BY j.alias, a.nom HAVING COUNT (c.id_avatar) > 1;
                
PROMPT  
PROMPT =======================================================
PROMPT Afficher les phrases persos et leurs avatars, + les avatars qui n'ont pas de phrase
PROMPT =======================================================

SELECT a.nom, nvl(p.phrase, 'Pas de phrase.')
    FROM phrasesPerso p FULL JOIN avatar a ON a.id = p.id_avatar
    ORDER BY a.nom;
                
PROMPT  
PROMPT =======================================================
PROMPT Liste des joueurs ayant acheté un item, avec le prix de l'item et le jeu auquel il appartient
PROMPT =======================================================

SELECT j.alias, a.montant, i.nom, z.nom
FROM joueur j JOIN achat a ON a.id_joueur = j.id
                JOIN items i ON i.id = a.id_item
                JOIN jeu z ON i.id_jeu = z.id;