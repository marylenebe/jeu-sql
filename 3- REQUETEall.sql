-- TOUTES LES REQU�TES --
PROMPT  
PROMPT =======================================================
PROMPT 1 - liste des joueurs : alias, date d�inscription. 
PROMPT =======================================================
SELECT alias, date_inscription
FROM joueur;

PROMPT  
PROMPT =======================================================
PROMPT 2 - Afficher tous les avatar du joueur principal
PROMPT =======================================================
SELECT a.nom,c.rouge,c.vert,c.bleu, a.date_creation
FROM avatar a
    FULL JOIN joueur j
        ON j.id = a.id_joueur
    FULL JOIN couleurs c
        ON a.id = c.id_avatar
WHERE j.alias = 'MrPaul';

SELECT alias, date_inscription
FROM joueur;
PROMPT  
PROMPT =======================================================
PROMPT 3- Afficher tous les CARACTERISTISTIQUES obtenu par l'Avatar du Joueur Principal
PROMPT =======================================================
SELECT c.nom, b.nom, a.date_obtention, a.niveau
FROM caracteristique_avatar a JOIN caracteristique b ON a.id_caracteristique = b.id
                            JOIN avatar c ON c.id = a.id 
                                    WHERE REGEXP_LIKE (c.nom, '*\*');
PROMPT  
PROMPT =======================================================
PROMPT 4- Donne la valeur totale de tous les items de l'avatar principal
PROMPT =======================================================
SELECT c.nom, SUM(b.cout) 
    FROM itemAvatar a 
        FULL JOIN items b 
            ON a.id_item = b.id
        FULL JOIN avatar c 
            ON c.id = a.id 
        WHERE REGEXP_LIKE (c.nom, '*\*')
        GROUP BY c.nom;
SELECT j.nom, i.nom
    FROM itemAvatar iA
        JOIN items i
            On iA.id_item=i.id
        JOIN avatar a
            ON iA.id_avatar = a.id;
PROMPT  
PROMPT =======================================================
PROMPT 5 - Nombre Total d''heure pass�es dans chaque jeu jou�.
PROMPT =======================================================
SELECT j.nom , SUM(tpj.duree)
	FROM temps_passe_jeu tpj
    JOIN activite a
        ON a.id = tpj.id_activite
    JOIN Jeu j
        ON tpj.id_jeu = j.id
    JOIN Joueur p
        ON p.id = a.id_joueur 
    WHERE
        p.alias = 'MrPaul'
    GROUP BY j.NOM;
                          
PROMPT  
PROMPT =======================================================         
PROMPT 6 -   SOMME DE TOUT LES ACHATS FAIT PAR LES JOUEURS
PROMPT =======================================================      
SELECT j.alias "Joueur", SUM(a.montant) "Montant depense"
    FROM joueur j
        JOIN achat a
            ON a.id_joueur = j.id
    GROUP BY j.alias;
PROMPT  
PROMPT =======================================================
PROMPT 7 -  tous les avatars qui poss�de plus de 1 item 
PROMPT =======================================================                
SELECT j.alias, a.nom
FROM joueur j JOIN avatar a ON j.id = a.id_joueur 
                JOIN itemAvatar c ON c.id_avatar = a.id
                GROUP BY j.alias, a.nom HAVING COUNT (c.id_avatar) > 1;
    
PROMPT  
PROMPT =======================================================
PROMPT 8a - 
PROMPT =======================================================
                


PROMPT  
PROMPT =======================================================
PROMPT 8c - Afficher les phrases persos et leurs avatars, + les avatars qui n'ont pas de phrase
PROMPT =======================================================

SELECT a.nom, nvl(p.phrase, 'Pas de phrase.')
    FROM phrasesPerso p FULL JOIN avatar a ON a.id = p.id_avatar
    ORDER BY a.nom;

