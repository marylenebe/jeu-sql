-- TOUTES LES REQU�TES --

PROMPT  
PROMPT =======================================================
PROMPT Afficher tous les avatar du joueur principal
PROMPT =======================================================
SELECT a.nom,c.rouge,c.vert,c.bleu, to_date(a.date_creation,'yyyy/mm/dd')
FROM avatar a
    JOIN joueur j
        ON j.id = id_joueur
    JOIN couleurs c
        ON a.id = c.id_avatar
WHERE j.alias = 'MrPaul' AND c.noCouleur = 1;

PROMPT  
PROMPT =======================================================
PROMPT Avatar principal: la valeur total de tout ses items
PROMPT =======================================================

SELECT SUM(cout*quantit�) "Valeur Total"
FROM avatar a
    JOIN itemAvatar
        ON a.id = id_avatar
    JOIN items
        ON id_item = items.id
WHERE a.nom = 'TheSuitedMan*';

PROMPT  
PROMPT =======================================================
PROMPT Le d�tail de chaque items obtenu par le joueur principal
PROMPT =======================================================

SELECT i.nom AS "Nom item", i.sigle AS "Sigle", i.cout AS "Valeur", i.description AS "Description"
FROM avatar a
    JOIN itemAvatar
        ON a.id = id_avatar
    JOIN items i
        ON id_item = i.id
WHERE id_joueur IN (SELECT id FROM joueur WHERE alias = 'MrPaul');