/*************************************************************
Auteurs: Marylène Beaudin, Hugo Bourcier, Vanessa Daneault-Pilote, Ludovic Doutre-Guay
Date de création: 02/10/2018
Description: Fichier de créations des tables
Projet: Base de données pour le jeu Mr. Paul's fantasies IX
**************************************************************/
--== EFFACER LES TABLES EXISTENTES ==--
DROP TABLE caracteristique_avatar CASCADE CONSTRAINTS;
DROP TABLE caracteristique CASCADE CONSTRAINTS;
DROP TABLE joueur CASCADE CONSTRAINTS;
DROP TABLE activite CASCADE CONSTRAINTS;
DROP TABLE avatar CASCADE CONSTRAINTS;
DROP TABLE couleurs CASCADE CONSTRAINTS;
DROP TABLE phrasesPerso CASCADE CONSTRAINTS;
DROP TABLE achat CASCADE CONSTRAINTS;  
DROP TABLE jeu CASCADE CONSTRAINTS;
DROP TABLE temps_passe_jeu CASCADE CONSTRAINTS;
DROP TABLE temps_passe_avatar CASCADE CONSTRAINTS;
DROP TABLE itemAvatar CASCADE CONSTRAINTS;
DROP TABLE items CASCADE CONSTRAINTS;
DROP SEQUENCE seq_avatar_id;
DROP SEQUENCE seq_couleur_id;
DROP SEQUENCE seq_phrasesPerso_id;


--== CREATION DES TABLES ==--
--Table joueur (contient toutes les informations d'un joueur)
    CREATE TABLE joueur (
        id		                NUMBER(10)         GENERATED ALWAYS AS IDENTITY,
        alias 	                VARCHAR2(32)       NOT NULL, 
        courriel	            VARCHAR2(128)      NOT NULL,
        mot_de_passe            VARCHAR2(32)       NOT NULL,
        genre                   VARCHAR2(1)        NOT NULL,
        date_inscription        DATE               DEFAULT CURRENT_TIMESTAMP,
        date_naissance          DATE,
        
        CONSTRAINT pk_joueur PRIMARY KEY (id),
        
        CONSTRAINT date_ins_valide CHECK (date_inscription BETWEEN TO_DATE('01/01/2018', 'dd/mm/yyyy') AND TO_DATE('01/01/2020', 'dd/mm/yyyy')), --Si date inscription entre 2018 et 2020
        CONSTRAINT date_n_valide CHECK ( date_naissance BETWEEN TO_DATE('01/01/1900', 'dd/mm/yyyy') AND TO_DATE('01/01/2020', 'dd/mm/yyyy')), --Si date inscription entre 1900 et 2020
        CONSTRAINT genre_joueur CHECK (UPPER(genre) = 'F' OR  UPPER(genre) = 'H' OR UPPER(genre) = 'X'), --Si genre Homme, femme, ou autre
        CONSTRAINT u_joueur_alias UNIQUE(alias),
        CONSTRAINT u_joueur_courriel UNIQUE(courriel),
        CONSTRAINT u_joueur_psw UNIQUE(mot_de_passe)
    );
--Creation des avatars
    CREATE TABLE avatar(
        id                      NUMBER(10), --UTILISER seq_avatar_id
        id_joueur               NUMBER              NOT NULL,
        nom                     VARCHAR2(32)        NOT NULL,
        nb_couleurs             NUMBER(1)           DEFAULT 1,
        date_creation           DATE                DEFAULT CURRENT_TIMESTAMP,
        nombre_mox              NUMBER(10)          DEFAULT 0,
        
        CONSTRAINT cc_mox CHECK (nombre_mox BETWEEN -1000000000 and 1000000000), --verification des moX
        CONSTRAINT cc_nb_couleurs CHECK (nb_couleurs <=3),
        CONSTRAINT u_avatar_nom UNIQUE(nom)
    );
--Creation des couleurs
    CREATE TABLE couleurs(
        id                      NUMBER(10), --UTILISER seq_couleur_id
        id_avatar               NUMBER              NOT NULL,
        noCouleur               NUMBER(1)           NOT NULL,
        rouge                   NUMBER(3)           NOT NULL,
        vert                    NUMBER(3)           NOT NULL,
        bleu                    NUMBER(3)           NOT NULL,
        
        CONSTRAINT cc_rouge CHECK(rouge BETWEEN 0 and 255), --verification que toutes les couleurs sont valides.
        CONSTRAINT cc_vert CHECK(vert BETWEEN 0 and 255),
        CONSTRAINT cc_bleu CHECK(bleu BETWEEN 0 and 255),
        CONSTRAINT cc_noCouleur CHECK (noCouleur <= 3)-- Pour que l'avatar ne puisse pas avoir plus de 3 couleurs
    );
    
CREATE TABLE phrasesPerso (
    id                      NUMBER, --UTILISER seq_phrasesPerso_id
    id_avatar               NUMBER              NOT NULL,
    phrase                  VARCHAR2(64)        NOT NULL
);

CREATE SEQUENCE seq_avatar_id
    INCREMENT BY 1
    START WITH 1;
    
CREATE SEQUENCE seq_couleur_id
    INCREMENT BY 1
    START WITH 1;
    
CREATE SEQUENCE seq_phrasesPerso_id
    INCREMENT BY 1
    START WITH 1;


CREATE TABLE caracteristique(
    id                      NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
    id_jeu                  NUMBER(1)           NOT NULL,
    nom                     VARCHAR2(32)        NOT NULL,
    sigle                   VARCHAR2(3)         NOT NULL,
    energie_acquisition     NUMBER(5,2)         NOT NULL,
    energie_utilisation     NUMBER(7,3)         NOT NULL,
    description             VARCHAR2(512),
    
    
    CONSTRAINT limit_energie_acquisition CHECK(energie_acquisition >= 25 AND energie_acquisition <= 250 ),
    CONSTRAINT limit_energie_utilisation CHECK(energie_utilisation >= -1000 AND energie_utilisation <= 1000 ),
    CONSTRAINT debut_sigle CHECK (sigle LIKE 'F%'),
    CONSTRAINT u_caracteristique_nom UNIQUE(nom),
    CONSTRAINT u_caracteristique_sigle UNIQUE(sigle)
);

CREATE TABLE caracteristique_avatar(
    id                      NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
    id_caracteristique      NUMBER(10)          NOT NULL,
    id_avatar               NUMBER(10)          NOT NULL,
    date_obtention          DATE                DEFAULT CURRENT_TIMESTAMP,
    niveau                  NUMBER(3)           DEFAULT 0
    
    CONSTRAINT niveau_max CHECK (niveau <= 999)
);

CREATE TABLE items (
	id		                NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
	nom     	            VARCHAR2(32)        NOT NULL, 
	id_jeu                  NUMBER(1)           NOT NULL,       
    sigle                   VARCHAR2(4)         NOT NULL,
    probabilite             NUMBER(9,8)         NOT NULL,
    cout                    NUMBER(2)           NOT NULL,
    description             VARCHAR2(512),
    
    CONSTRAINT pk_idItem PRIMARY KEY (id),
    CONSTRAINT u_items_nom UNIQUE(nom),
    CONSTRAINT u_items_sigle UNIQUE(sigle),
    CONSTRAINT cc_sigle CHECK (sigle LIKE 'I___')
);

CREATE TABLE itemAvatar (
    id                      NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
    id_item                 NUMBER(10)          NOT NULL,
    id_avatar               NUMBER(10)          NOT NULL,
    dateObtention           DATE                DEFAULT CURRENT_TIMESTAMP,
    quantite               NUMBER(3),
    
    CONSTRAINT pk_idItemAvatar PRIMARY KEY (id)
);

CREATE TABLE jeu (
    id                      NUMBER(1)           GENERATED ALWAYS AS IDENTITY,
    nom                     VARCHAR2(16),
    sigle                   VARCHAR2(6) ,
    description             VARCHAR2(2048),
    
    CONSTRAINT pk_jeu PRIMARY KEY (id)
);

CREATE TABLE achat (
    id                      NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
    id_item                 NUMBER(10),
    id_joueur               NUMBER(10),
    date_paiement           DATE                DEFAULT CURRENT_TIMESTAMP NOT NULL,
    mode_paiement           VARCHAR2(10)        NOT NULL,
    montant                 NUMBER(6,2)         NOT NULL,
    type_achat              VARCHAR2(16)        NOT NULL,
    id_intervalle           NUMBER(10),
    date_debut              DATE,
    date_fin                DATE,
    
    CONSTRAINT pk_achats PRIMARY KEY (id),
    
    CONSTRAINT intervalle_montants CHECK (montant BETWEEN 0.01 AND 2500.00),
    CONSTRAINT type_pa CHECK(type_achat = 'temps' OR type_achat = 'item'),
    CONSTRAINT paiements CHECK 
        (mode_paiement = 'credit' OR mode_paiement = 'paypal' OR mode_paiement = 'interact'),
    CONSTRAINT intervalles CHECK(date_debut<date_fin)
);

CREATE TABLE temps_passe_avatar(
    id                      NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
    id_avatar               NUMBER(10)          NOT NULL,
	id_activite		        NUMBER(10)          NOT NULL,
    debut_activite          DATE                NOT NULL,
    duree                   NUMBER (8,2)        NOT NULL,
    
    CONSTRAINT pk_tempsPasseAvatar_id PRIMARY KEY (id)
);

CREATE TABLE temps_passe_jeu(
    id                      NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
    id_jeu                  NUMBER(10)          NOT NULL,
    id_avatar               NUMBER(10)          NOT NULL,
	id_activite		        NUMBER(10)          NOT NULL,
    debut_activite          DATE                NOT NULL,               
    duree                   NUMBER (8,2)        NOT NULL,
    
    CONSTRAINT pk_tempsPasseJeu_id PRIMARY KEY (id)
);
CREATE TABLE activite(
    id                      NUMBER(10)          GENERATED ALWAYS AS IDENTITY,
    id_joueur               NUMBER(10),
    debutActivite           DATE                DEFAULT CURRENT_TIMESTAMP,
    dureeActivite           NUMBER(10)          NOT NULL,
    
    CONSTRAINT pk_activite_id PRIMARY KEY (id)
);

ALTER TABLE temps_passe_avatar
	ADD CONSTRAINT fk_tempsPasseAvatar_idActivite FOREIGN KEY (id_activite) REFERENCES activite(id);
ALTER TABLE temps_passe_jeu
	ADD CONSTRAINT fk_tempsPasseJeu_idActivite FOREIGN KEY (id_activite) REFERENCES activite(id);
	
ALTER TABLE activite
    ADD CONSTRAINT fk_activite_idJoueur FOREIGN KEY (id_joueur) REFERENCES joueur(id);


ALTER TABLE avatar
    ADD CONSTRAINT pk_avatar PRIMARY KEY(id);
ALTER TABLE avatar
    ADD CONSTRAINT fk_avatar_joueur FOREIGN KEY (id_joueur) REFERENCES joueur(id);

ALTER TABLE couleurs
    ADD CONSTRAINT pk_couleur PRIMARY KEY(id);
ALTER TABLE couleurs
    ADD CONSTRAINT fk_couleur_avatar FOREIGN KEY (id_avatar) REFERENCES avatar(id);

ALTER TABLE phrasesPerso
    ADD CONSTRAINT pk_phrasesPerso PRIMARY KEY(id);
ALTER TABLE phrasesPerso
    ADD CONSTRAINT fk_phrasesPerso_joueur FOREIGN KEY (id_avatar) REFERENCES avatar(id);


ALTER TABLE caracteristique
    ADD CONSTRAINT fk_jeu_id FOREIGN KEY (id_jeu) REFERENCES jeu(id);
ALTER TABLE caracteristique
    ADD CONSTRAINT pk_caracteristique_id PRIMARY KEY (id);
ALTER TABLE caracteristique_avatar
    ADD CONSTRAINT fk_avatar_id FOREIGN KEY (id_avatar) REFERENCES avatar(id);
ALTER TABLE caracteristique_avatar
    ADD CONSTRAINT pk_caracteristiqueAvatar_id PRIMARY KEY (id);
ALTER TABLE caracteristique_avatar
    ADD CONSTRAINT fk_caracteristique_id FOREIGN KEY (id_caracteristique) REFERENCES caracteristique(id);
ALTER TABLE itemAvatar
    ADD CONSTRAINT fk_item_avatar FOREIGN KEY (id_avatar) REFERENCES avatar(id);
    
ALTER TABLE temps_passe_jeu
    ADD CONSTRAINT fk_tempsjeu_id FOREIGN KEY (id_jeu) REFERENCES jeu(id);
    
ALTER TABLE temps_passe_avatar
    ADD CONSTRAINT fk_tempsavatar_id FOREIGN KEY (id_avatar) REFERENCES avatar(id);
    
ALTER TABLE items ADD CONSTRAINT item_jeu FOREIGN KEY (id_jeu) REFERENCES jeu(id);
ALTER TABLE itemAvatar ADD CONSTRAINT item_item FOREIGN KEY (id_item) REFERENCES items(id);
ALTER TABLE achat ADD CONSTRAINT achat_joueur FOREIGN KEY (id_joueur) REFERENCES joueur(id);
ALTER TABLE achat ADD CONSTRAINT achat_item FOREIGN KEY (id_item) REFERENCES items(id);




