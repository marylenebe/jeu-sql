/*************************************************************
Auteurs: Maryl�ne Beaudin, Hugo Bourcier, Vanessa Daneault-Pilote, Ludovic Doutre-Guay
Date de creation: 02/10/2018
Description: Fichier des requ�tes
Projet: Base de donnees pour le jeu Mr. Paul's fantasies IX
**************************************************************/
-- TOUTES LES REQU�TES --
PROMPT  
PROMPT =======================================================
PROMPT 1 - liste des joueurs : alias, date d�inscription. 
PROMPT =======================================================
SELECT alias, date_inscription
FROM joueur;

PROMPT  
PROMPT =======================================================
PROMPT 2 - Afficher tous les avatar du joueur principal
PROMPT =======================================================
SELECT a.nom,c.rouge,c.vert,c.bleu, to_char(a.date_creation,'yyyy-mm-dd')
FROM avatar a
    FULL JOIN joueur j
        ON j.id = a.id_joueur
    FULL JOIN couleurs c
        ON a.id = c.id_avatar
WHERE j.alias = 'MrPaul' ;
PROMPT  
PROMPT =======================================================
PROMPT 3- Afficher tous les CARACTERISTISTIQUES obtenu par l'Avatar du Joueur Principal
PROMPT =======================================================
SELECT c.nom, b.nom, a.date_obtention, a.niveau
FROM caracteristique_avatar a JOIN caracteristique b ON a.id_caracteristique = b.id
                            JOIN avatar c ON c.id = a.id 
                                    WHERE REGEXP_LIKE (c.nom, '*\*');
PROMPT  
PROMPT =======================================================
PROMPT 4-Avatar principal: la valeur total de tout ses items
PROMPT =======================================================

SELECT SUM(cout*quantite) "Valeur Total"
FROM avatar a
    JOIN itemAvatar
        ON a.id = id_avatar
    JOIN items
        ON id_item = items.id
WHERE a.nom = 'TheSuitedMan*';
PROMPT  
PROMPT =======================================================
PROMPT 5 - Nombre Total d''heure passees dans chaque jeu joue.
PROMPT =======================================================
SELECT j.nom , SUM(tpj.duree)
	FROM temps_passe_jeu tpj
    JOIN activite a
        ON a.id = tpj.id_activite
    JOIN Jeu j
        ON tpj.id_jeu = j.id
    JOIN Joueur p
        ON p.id = a.id_joueur 
    WHERE
        p.alias = 'MrPaul'
    GROUP BY j.NOM;
                          
PROMPT  
PROMPT =======================================================         
PROMPT 6 -   SOMME DE TOUT LES ACHATS FAIT PAR LES JOUEURS
PROMPT =======================================================      
SELECT j.alias "Joueur", SUM(a.montant) "Montant depenser"
    FROM joueur j
        JOIN achat a
            ON a.id_joueur = j.id
    GROUP BY j.alias;
PROMPT  
PROMPT =======================================================
PROMPT 7 -  tous les avatars qui poss�de plus de 1 item 
PROMPT =======================================================                
SELECT j.alias, a.nom
FROM joueur j JOIN avatar a ON j.id = a.id_joueur 
                JOIN itemAvatar c ON c.id_avatar = a.id
                GROUP BY j.alias, a.nom HAVING COUNT (c.id_avatar) > 1;
    
PROMPT=======================================================
PROMPT 8a - Le detail de chaque items obtenu par le joueur principal
PROMPT =======================================================

SELECT i.nom AS "Nom item", i.sigle AS "Sigle", i.cout AS "Valeur", i.description AS "Description"
FROM avatar a
    JOIN itemAvatar
        ON a.id = id_avatar
    JOIN items i
        ON id_item = i.id
WHERE id_joueur IN (SELECT id FROM joueur WHERE alias = 'MrPaul');
    
 
PROMPT  
PROMPT =======================================================
PROMPT 8b - Liste des joueurs ayant achete un item, avec le prix de l'item et le jeu auquel il appartient
PROMPT =======================================================

SELECT j.alias, a.montant, i.nom, z.nom
FROM joueur j JOIN achat a ON a.id_joueur = j.id
                JOIN items i ON i.id = a.id_item
                JOIN jeu z ON i.id_jeu = z.id;

PROMPT  
PROMPT =======================================================
PROMPT 8c - Afficher les phrases persos et leurs avatars, + les avatars qui n'ont pas de phrase
PROMPT =======================================================

SELECT a.nom, nvl(p.phrase, 'Pas de phrase.')
    FROM phrasesPerso p FULL JOIN avatar a ON a.id = p.id_avatar
    ORDER BY a.nom;