Auteurs: Maryl�ne Beaudin, Hugo Bourcier, Vanessa Daneault-Pilote, Ludovic Doutre-Guay
Date de creation: 02/10/2018
Description: Fichier d'insertions des enregistrements
Projet: Base de donnees pour le jeu Mr. Paul's fantasies IX
**************************************************************/
--==    L E S   J E U X    ==--
    --JEU OrionX
    INSERT INTO jeu  (Nom,Sigle,Description)
        VALUES ('OrionX game',
        'OrionX',
        'OrionX est un jeu de transport spacial. Celui qui recoltera le plus de ressources, gagnera');
            --ITEM de OrionX
            INSERT INTO items(nom, id_Jeu,sigle, probabilite, cout, description)
                VALUES ('eclat de Plan�te',
                        (SELECT id FROM jeu WHERE nom='OrionX game'),
                        'IEPO',
                        1.20,12,
                        'Explosif construit � base de morceaux de plan�tes (Utilisation 1 fois)');    
            INSERT INTO items(nom, id_Jeu,sigle, probabilite, cout, description)
                VALUES ('Rayon de la mort',
                    (SELECT id FROM jeu WHERE nom='OrionX game'),
                    'IRMO',1.02,
                    80,'Tout le monde meurt dans le monde (Utilisation max : 1 fois par semaine)');
            INSERT INTO items(nom, id_Jeu,sigle, probabilite, cout, description) 
                VALUES ('Casque Spatial',
                    (SELECT id FROM jeu WHERE nom='OrionX game'),
                    'ICSO',
                    1.30,
                    10,
                    'Regeneration compl�te de la vie. (Utilisation 1 fois)');
                
            --CARACTERISTIQUES de OrionX
            INSERT INTO caracteristique(id_jeu,nom,sigle,energie_acquisition,energie_utilisation, description)
                VALUES ((SELECT id FROM jeu WHERE nom='OrionX game'),
                    'Absorption Mana',
                    'FAM',
                    200,
                    500,
                    'Abosorbe 500 poionts de Mana du joueur adverses');
            INSERT INTO caracteristique(id_jeu,nom,sigle,energie_acquisition,energie_utilisation, description)
                VALUES ((SELECT id FROM jeu WHERE nom='OrionX game'),
                    'Controle Gravite',
                    'FCG',
                    200,
                    500,
                    'Permet de contr�ler la gravite autour de soi. (Faire voler soi-m�me ou des objets autour.)');
                
    --JEU DeepHorizonX
    INSERT INTO jeu(nom, sigle, description)
        VALUES('DeepHorizonX',
        'deepho',
        'Monde immerge sous l''eau o� on doit se rendre dans une epave au fond');
        
        --ITEM de DeepHorizonX
            INSERT INTO items(nom,id_Jeu,sigle,probabilite,cout,description) 
                VALUES('Palmes dagilite',
                    (SELECT id FROM jeu WHERE nom = 'DeepHorizonX'),
                    'Ipal',
                    0.6,
                    6,
                    ' Permets plus de vitesse et de fluidite dans l''eau');
            INSERT INTO items(nom,id_Jeu,sigle,probabilite,cout,description) 
                VALUES('Branchies',
                    (SELECT id FROM jeu WHERE nom = 'DeepHorizonX'),
                    'Ibra',
                    0.2,
                    40,
                    ' Permets de respire sous l''eau en tout temps');
            INSERT INTO items(nom,id_Jeu,sigle,probabilite,cout,description)
                VALUES('Sonar',
                    (SELECT id FROM jeu WHERE nom = 'DeepHorizonX'),
                    'Ison',
                    0.9,
                    1,
                    'Detecte les ennemis (1 utilisation)');
                    
            --CARACTERISTIQUES de DeepHorizonX        
            INSERT INTO caracteristique(id_jeu,nom,sigle,energie_acquisition,energie_utilisation,description)
                VALUES((SELECT id FROM jeu WHERE nom = 'DeepHorizonX'),
                    'auto-guerison',
                    'Fau',
                    100,
                    500,
                    'Permet de se guerir soit-m�me');
            INSERT INTO caracteristique(id_jeu,nom,sigle,energie_acquisition,energie_utilisation,description)
                VALUES((SELECT id FROM jeu WHERE nom = 'DeepHorizonX'),
                    'DecuplerForce', 
                    'Fde',
                    200,
                    100,
                    'Permet de decupler sa force');
            
    
    --JEU FreeZoneX
    INSERT INTO jeu (nom, sigle, description)
        VALUES (    'FreeZoneX',
                    'FreeZo',
                    'Le jeu FreeZoneX se deroule dans un univers chaotique
                    o� r�gne le non-sens, ainsi que le licornes. 
                    Ses r�gles totalement incoherentes 
                    permettent aux joueurs de faire absolument n''importe quoi.');
                    
            --ITEM de FreeZoneX        
            INSERT INTO items (id_jeu, nom, sigle, probabilite, cout, description)
                VALUES (    (SELECT id FROM jeu WHERE nom LIKE 'FreeZoneX'),
                            'Super Boots Jump',
                            'ISBJ',
                            0.1,
                            75,
                            'Bottes qui permettent de sauteur haut. Vraiment haut. Abusivement haut.'); 
            INSERT INTO items (id_jeu, nom, sigle, probabilite, cout, description)
                VALUES (    (SELECT id FROM jeu WHERE nom LIKE 'FreeZoneX'),
                            'Jets de gomme balloune',
                            'IJGB',
                            0.7,
                            3,
                            'Gomme qui emprisonne les ennemis. Particuli�rement efficace contre
                            les diabetiques.');
                               
            INSERT INTO items (id_jeu, nom, sigle, probabilite, cout, description)
                VALUES (    (SELECT id FROM jeu WHERE nom LIKE 'FreeZoneX'),
                            'Licorne Sauvage',
                            'ILCS',
                            0.1,
                            50,
                            'Allie qui dure tout le temps de l''activite.');                
            --CARACTERISTIQUES de FreeZoneX
            
                        INSERT INTO caracteristique (id_jeu, nom, sigle, energie_acquisition, energie_utilisation, description)
                VALUES (    (SELECT id FROM jeu WHERE nom LIKE 'FreeZoneX'),
                            'Capacite d''ubiquite',
                            'FCU',
                            200,
                            100,
                            'Permet au joueur de se multiplier pour �tre � plusieurs endroits � la fois,
                            creant un "lag" monumental et generalise sur le reseau.');
                    INSERT INTO caracteristique (id_jeu, nom, sigle, energie_acquisition, energie_utilisation, description)
                VALUES (    (SELECT id FROM jeu WHERE nom LIKE 'FreeZoneX'),
                            'Teleportation',
                            'FTE',
                            250,
                            120,
                            'Permet au joueur de se teleporter.');
            
    --JEU DreamX                        
    INSERT INTO jeu (nom,sigle,description) 
        VALUES ('DreamX',
            'DREAMX',
            'Jeu se passant dans le monde des r�ves. Attention au mauvais r�ves!!');
            
           --ITEMS du DreamX  
            INSERT INTO items (nom,id_jeu,sigle,probabilite,cout,description)
                VALUES ('Bo�te de cauchemars',
                    (SELECT id FROM jeu WHERE nom='DreamX'),
                    'ICAU',
                    0.8,
                    3,
                    'Paralyze un adversaire'); 
            INSERT INTO items (nom,id_jeu,sigle,probabilite,cout,description)
                VALUES ('Kaleidoscope magique',
                    (SELECT id FROM jeu WHERE nom='DreamX'),
                    'Ikal',
                    0.1,
                    50,
                    'Vois les prochaines vagues d''ennemis � travers celui-ci');
            INSERT INTO items (nom,id_jeu,sigle,probabilite,cout,description)
                VALUES ('Luther King''s ring',
                    (SELECT id FROM jeu WHERE nom='DreamX'),
                    'Ilut',
                    0.4,
                    8,
                    'Rage mode activated (10 secondes)');
                    
            --CARACTERISTIQUES du DreamX     
            INSERT INTO caracteristique (id_jeu,nom,sigle,energie_acquisition,energie_utilisation, description)
                VALUES ((SELECT id from jeu WHERE nom ='DreamX'),
                    'Ralentir le temps',
                    'Fca', 
                    75,
                    -100,
                    'Le temps est ralenti de 30 %');
            INSERT INTO caracteristique (id_jeu,nom,sigle,energie_acquisition,energie_utilisation, description)
                VALUES ((SELECT id from jeu WHERE nom ='DreamX'),
                    'Invisibilite',
                    'Fin',
                    123,
                    560,
                    'Invisible (10 secondes)');
 
--==    L E S   J O U E U R S    ==--  
 
    --LUDOVIC    
    INSERT INTO joueur (alias, courriel, mot_de_passe, genre,date_naissance) VALUES ('Vidar389', 'ludovicdguay@gmail.com', 'M0td3p455e', 'H', TO_DATE('15-09-1996','dd/mm/yyyy'));
        
        --AVATARS Vidar389
            INSERT INTO avatar (id,id_joueur,nom,nombre_mox,date_creation)
                VALUES (seq_avatar_id.NEXTVAL,
                        (SELECT id FROM joueur WHERE alias='Vidar389'),
                        'Vidar',
                        50,
                        to_date('25/12/2017','dd/mm/yyyy'));
            INSERT INTO avatar (id,id_joueur,nom,nombre_mox,date_creation)
                VALUES (seq_avatar_id.NEXTVAL,
                        (SELECT id FROM joueur WHERE alias='Vidar389'),
                        'Fenrir',
                        30,
                        to_date('25/12/2017','dd/mm/yyyy'));
            INSERT INTO avatar (id,id_joueur,nom,nombre_mox,date_creation)
                VALUES (seq_avatar_id.NEXTVAL,
                        (SELECT id FROM joueur WHERE alias='Vidar389'),
                        'Midgard',
                        60,
                        to_date('25/12/2017','dd/mm/yyyy'));
                        
        --CARACTERISTIQUES Vidar389 
            INSERT INTO caracteristique_avatar(id_caracteristique,id_avatar,niveau)
                VALUES((SELECT id FROM caracteristique WHERE sigle = 'Fau'),
                        (SELECT id FROM avatar WHERE nom = 'Fenrir'),
                        17);
            INSERT INTO caracteristique_avatar(id_caracteristique,id_avatar,niveau) 
                VALUES ((SELECT id FROM caracteristique WHERE nom='Absorption Mana'),
                        (SELECT id FROM avatar WHERE nom='Vidar'),
                        1);
                        
        --PHRASES PERSO Vidar389    
            INSERT INTO phrasesperso(id,id_avatar,phrase) 
                VALUES (seq_phrasesPerso_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='Vidar'),
                        'Ceux qui vont mourrir, te salue');
            INSERT INTO phrasesperso(id,id_avatar,phrase) 
                VALUES (seq_phrasesPerso_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='Vidar'),
                        'Prepare to die.');
            INSERT INTO phrasesperso(id,id_avatar,phrase)
                VALUES (seq_phrasesPerso_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='Vidar'),
                        'Le dieu de la Vengeance se prepare.');
                        
        --COULEURS Vidar389 
            INSERT INTO couleurs (id,id_avatar,noCouleur, rouge, vert,bleu)
                    VALUES (seq_couleur_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom='Vidar'),
                            1,
                            181,
                            75,
                            27);
            INSERT INTO couleurs (id,id_avatar,noCouleur, rouge, vert,bleu)
                    VALUES (seq_couleur_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom='Vidar'),
                            2,
                            24,
                            175,
                            145);
            INSERT INTO couleurs (id,id_avatar,noCouleur, rouge, vert,bleu)
                    VALUES (seq_couleur_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom='Vidar'),
                            3,
                            42,
                            170,
                            81);
        
    --M. PAUL
    INSERT INTO joueur (alias, courriel, mot_de_passe, genre,date_naissance)
        VALUES ('MrPaul',
                'monsieurpaul@gmail.com',
                'secret1',
                'H',
                TO_DATE('20-08-1985','dd/mm/yyyy'));
        --AVATARS MrPaul
            INSERT INTO avatar (id,id_joueur,nom,nombre_mox,date_creation)
                VALUES (seq_avatar_id.NEXTVAL,
                        (SELECT id FROM joueur WHERE alias='MrPaul'),
                        'TheSuitedMan*',
                        200,
                        to_date('25/12/2017','dd/mm/yyyy'));
            INSERT INTO avatar (id,id_joueur,nom,nombre_mox,date_creation) 
                VALUES (seq_avatar_id.NEXTVAL,
                        (SELECT id FROM joueur WHERE alias='MrPaul'),
                        'DoctorC++',
                        300,
                        to_date('25/12/2017','dd/mm/yyyy'));
            INSERT INTO avatar (id,id_joueur,nom,nombre_mox,date_creation)
                VALUES (seq_avatar_id.NEXTVAL,
                        (SELECT id FROM joueur WHERE alias='MrPaul'),
                        'PlayerNumber1',
                        600,
                        to_date('25/12/2017','dd/mm/yyyy'));
        --PHRASES PERSO Mr Paul     
            INSERT INTO phrasesperso(id,id_avatar,phrase) 
                VALUES (seq_phrasesPerso_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                        'hhmmm bon.');
            INSERT INTO phrasesperso(id,id_avatar,phrase)
                VALUES (seq_phrasesPerso_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                        'N''oublier pas les commentaires.');
            INSERT INTO phrasesperso(id,id_avatar,phrase)
                VALUES (seq_phrasesPerso_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                        'LaStruct.');
        
        --CARACTERISTIQUES Mr Paul              
            INSERT INTO caracteristique_avatar(id_caracteristique,id_avatar,niveau) 
                VALUES ((SELECT id FROM caracteristique WHERE nom='Absorption Mana'),
                        (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                        999);
            INSERT INTO caracteristique_avatar(id_caracteristique,id_avatar,niveau)
                VALUES ((SELECT id FROM caracteristique WHERE nom='Controle Gravite'),
                        (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                        999);
            INSERT INTO caracteristique_avatar(id_caracteristique,id_avatar,niveau)
                VALUES((SELECT id FROM caracteristique WHERE sigle = 'Fau'),
                        (SELECT id FROM avatar WHERE nom = 'DoctorC++'),
                        88);
        
        --ITEMS Mr Paul      
            INSERT INTO itemavatar(id_item,id_avatar,quantite) 
                VALUES ((SELECT id FROM items WHERE nom='Rayon de la mort'),
                        (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                        5);
            INSERT INTO itemAvatar(id_Item,id_Avatar,quantite) 
                VALUES((SELECT id FROM items WHERE sigle = 'ILCS'),
                        (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                        2);
        
        --COULEURS Mr Paul     
            INSERT INTO couleurs (id,id_avatar,noCouleur, rouge, vert,bleu)
                VALUES (seq_couleur_id.NEXTVAL,(SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                        1,
                        133,
                        82,
                        51);
            INSERT INTO couleurs (id,id_avatar,noCouleur, rouge, vert,bleu) 
                VALUES (seq_couleur_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='DoctorC++'),
                        2,
                        182,
                        77,
                        56);
            INSERT INTO couleurs (id,id_avatar,noCouleur, rouge, vert,bleu) 
                VALUES (seq_couleur_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='PlayerNumber1'),
                        3,
                        67,
                        42,
                        38);
        
        --ACHATS Mr Paul       
            INSERT INTO achat (id_item, id_joueur, mode_paiement,montant, type_achat) 
                VALUES ((SELECT id FROM items WHERE nom='eclat de Plan�te'),
                        (SELECT id FROM joueur WHERE alias='MrPaul'),
                        'paypal',
                        2,
                        'item');
            INSERT INTO achat (id_joueur, mode_paiement, montant, type_achat, date_debut, date_fin)
                VALUES ((SELECT id FROM joueur WHERE alias LIKE 'MrPaul'),
                       'credit',
                        30,
                        'temps',
                        TO_DATE('05/06/2019', 'dd/mm/yyyy'),
                        TO_DATE('05/07/2019', 'dd/mm/yyyy'));
        
    --MARYL�NE
    INSERT INTO joueur(alias,courriel,mot_de_passe,date_naissance,genre)
        VALUES('mary',
                'meandfun@hotmail.com',
                'gswGSW689',
                to_date('13/05/1991','dd/mm/yyyy'),
                'F');
        --AVATARS mary
            INSERT INTO avatar(id,id_joueur,nom,nombre_mox,date_creation) 
                VALUES(seq_avatar_id.nextval,
                        (SELECT id FROM joueur WHERE alias = 'mary'),
                        'potterhead',
                        100,
                        to_date('01/06/2017','dd/mm/yyyy'));
        --COULEURS mary
            INSERT INTO couleurs(id,id_avatar,noCouleur,rouge,vert,bleu) 
                VALUES(seq_couleur_id.nextval,
                        (SELECT id FROM avatar WHERE nom = 'potterhead'), 
                        1,
                        255,
                        102,
                        51);
        --PHRASES PERSO mary
            INSERT INTO phrasesPerso(id,id_avatar,phrase)
                VALUES(seq_phrasesPerso_id.nextval,
                        (SELECT id FROM avatar WHERE nom = 'potterhead'),
                        'U aRe A WIzaRd HaRrY');
        --ACHATS mary
            INSERT INTO achat (id_joueur, mode_paiement, montant, type_achat, date_debut, date_fin)
                VALUES (    (SELECT id FROM joueur WHERE alias LIKE 'mary'),
                           'interact',
                            200,
                            'temps',
                            TO_DATE('01/06/2017', 'dd/mm/yyyy'),
                            TO_DATE('01/06/2018', 'dd/mm/yyyy'));
        
    --HUGO
    INSERT INTO joueur (alias, courriel, mot_de_passe, genre, date_naissance)
        VALUES (        'HugoBourcier', 
                        'hugo.bourcier1212@gmail.com', 
                        'AAAaaa111', 
                        'h', 
                        TO_DATE('12/12/1990','dd/mm/yyyy'));
        --AVATAR HugoBourcier
            INSERT INTO avatar (id, id_joueur, nom, nb_couleurs,date_creation)
                VALUES (     seq_avatar_id.NEXTVAL, 
                             (SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier'), 
                            'Moi', 
                             '2',
                        to_date('25/12/2017','dd/mm/yyyy'));
        --COULEUR HugoBourcier    
            INSERT INTO couleurs (id, id_avatar, noCouleur, rouge, vert, bleu)
                VALUES (    seq_couleur_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            1,
                            35,
                            240,
                            100);
            INSERT INTO couleurs (id, id_avatar, noCouleur, rouge, vert, bleu)
                VALUES (    seq_couleur_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            2,
                            122,
                            166,
                            98);
        --PHRASES PERSO HugoBourcier                
            INSERT INTO phrasesPerso (id, id_avatar, phrase)
                VALUES (    seq_phrasesPerso_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            'Pour le fort rien n''est plus dangereux que la pitie.');
        --CARACTERISTIQUES HugoBourcier           
            INSERT INTO caracteristique_avatar (id_caracteristique, id_avatar, niveau)
                VALUES (    (SELECT id FROM caracteristique WHERE nom LIKE 'Capacite d''ubiquite'),
                            (SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                               200);
            INSERT INTO caracteristique_avatar (id_caracteristique, id_avatar, niveau)
                VALUES (    (SELECT id FROM caracteristique WHERE nom LIKE 'Capacite d''ubiquite'),
                            (SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            200);
        --ITEMS HugoBourcier 
            INSERT INTO itemAvatar (id_item, id_avatar, quantite)
                VALUES (    (SELECT id FROM items WHERE nom LIKE 'Super Boots Jump'),
                            (SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            2);
                            
            INSERT INTO itemAvatar(id_Item,id_Avatar,quantite)
                VALUES((SELECT id FROM items WHERE sigle = 'Ibra'),
                        (SELECT id FROM avatar WHERE nom = 'Moi'),
                        1);
            INSERT INTO itemAvatar(id_Item,id_Avatar,quantite) 
                VALUES((SELECT id FROM items WHERE sigle = 'Ison'),
                        (SELECT id FROM avatar WHERE nom = 'Moi'),
                        1); 
        --ACHATS HugoBourcier            
            INSERT INTO achat (id_item, id_joueur, mode_paiement, montant, type_achat)
                VALUES (    (SELECT id FROM items WHERE nom LIKE 'Super Boots Jump'),
                            (SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier'),
                            'credit',
                            (SELECT cout FROM items WHERE nom LIKE 'Super Boots Jump'),
                            'item');
                            
            INSERT INTO achat (id_joueur, mode_paiement, montant, type_achat, date_debut, date_fin)
                VALUES (    (SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier'),
                           'paypal',
                            200,
                            'temps',
                            TO_DATE('01/01/2018', 'dd/mm/yyyy'),
                            TO_DATE('01/01/2019', 'dd/mm/yyyy'));
                            
                            
    --VANESSA      
    INSERT INTO joueur (alias,courriel,mot_de_passe,genre,date_naissance) VALUES ('petit-boursier1212','petit-boursier1212@gmail.com','fall2018','h',TO_DATE('19/11/1990','dd/mm/yyyy'));
         --AVATAR 1 petit-boursier1212       
            INSERT INTO avatar (id,id_joueur,nom,nombre_mox,date_creation)
                VALUES (seq_avatar_id.NEXTVAL,
                        (SELECT id FROM joueur WHERE alias='petit-boursier1212'),
                        'mauvais_herbe',
                        15555,
                        to_date('02/11/2017','dd/mm/yyyy'));
            --PHRASE PERSO petit-boursier1212   
                INSERT INTO phrasesPerso (id,id_avatar,phrase)
                VALUES (seq_phrasesPerso_id.NEXTVAL,
                        (SELECT id FROM avatar WHERE nom='mauvais_herbe'),
                        'The world ain''t all sunshine and rainbows - Rocky');
            --COULEUR petit-boursier1212           
                INSERT INTO couleurs (id,id_avatar,noCouleur,rouge,vert,bleu)
                    VALUES (seq_couleur_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom ='mauvais_herbe'),
                            1,
                            242,
                            158,
                            45);
                INSERT INTO couleurs (id,id_avatar,noCouleur,rouge,vert,bleu)
                    VALUES (seq_couleur_id.NEXTVAL,
                            (SELECT id FROM avatar WHERE nom ='mauvais_herbe'),
                            2,
                            249,
                            138,
                            249);
            --CARACTERISTIQUES petit-boursier1212
                INSERT INTO caracteristique_avatar(id_caracteristique,id_avatar,niveau)
                    VALUES ((SELECT id FROM caracteristique WHERE nom='Invisibilite'),
                            (SELECT id FROM avatar WHERE nom='mauvais_herbe'),
                            1);
                INSERT INTO itemavatar(id_item,id_avatar,quantite)
                    VALUES ((SELECT id FROM items WHERE nom='Luther King''s ring'),
                            (SELECT id FROM avatar WHERE nom='mauvais_herbe'),
                            13);
             --ACHAT petit-boursier1212
                INSERT INTO achat (id_joueur, mode_paiement, montant, type_achat, date_debut, date_fin)
                    VALUES ((SELECT id FROM joueur WHERE alias LIKE 'petit-boursier1212'),
                           'paypal',
                            30,
                            'temps',
                            TO_DATE('30/11/2017', 'dd/mm/yyyy'),
                            TO_DATE('30/12/2017', 'dd/mm/yyyy'));
 --==    L E S   A C T I V I T e S    ==--                            
    --ACTIVITe mary
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite) 
            VALUES((SELECT id FROM joueur WHERE alias = 'mary'),
                    to_date('05/06/2017','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'mary'),
                    to_date('07/09/2017','dd/mm/yyyy'),
                    1200);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'mary'),
                    to_date('12/12/2017','dd/mm/yyyy'),
                    3800);
        -- Temps passer par avatar et par monde de mary
            INSERT INTO temps_passe_avatar(id_avatar,id_activite,debut_activite, duree) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'potterhead'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'mary') AND debutActivite=to_date('05/06/2019','dd/mm/yyyy')),
                            TO_DATE('05/06/2017', 'dd/mm/yyyy'),
                            3600);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite,debut_activite, duree) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'potterhead'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'mary')AND debutActivite=to_date('07/06/2019','dd/mm/yyyy')),
                            TO_DATE('07/09/2017', 'dd/mm/yyyy'),
                            1200);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite,debut_activite, duree) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'potterhead'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'mary')AND debutActivite=to_date('12/06/2019','dd/mm/yyyy')),
                            TO_DATE('12/12/2017', 'dd/mm/yyyy'),
                            3800);
            
            INSERT INTO temps_passe_jeu(id_jeu,id_avatar,id_activite,debut_activite, duree) 
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                (SELECT id FROM avatar WHERE nom = 'potterhead'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'mary')AND debutActivite=to_date('05/06/2019','dd/mm/yyyy')),
                            TO_DATE('05/06/2017', 'dd/mm/yyyy'),
                            3600);
            INSERT INTO temps_passe_jeu(id_jeu,id_avatar,id_activite,debut_activite, duree) 
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'DREAMX'),
                (SELECT id FROM avatar WHERE nom = 'potterhead'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'mary')AND debutActivite=to_date('07/06/2019','dd/mm/yyyy')),
                            TO_DATE('07/09/2017', 'dd/mm/yyyy'),
                            1200);
            INSERT INTO temps_passe_jeu(id_jeu,id_avatar,id_activite,debut_activite, duree) 
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'OrionX'),
                (SELECT id FROM avatar WHERE nom = 'potterhead'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'mary')AND debutActivite=to_date('12/06/2019','dd/mm/yyyy')),
                            TO_DATE('12/12/2017', 'dd/mm/yyyy'),
                            3800);
                        
    --ACTIVITe Vidar389 
    INSERT INTO achat (id_joueur, mode_paiement,montant, type_achat, date_debut, date_fin) 
        VALUES ((SELECT id FROM joueur WHERE alias='Vidar389'),
                'paypal',
                350,
                'temps',
                to_date('28-09-2018','DD-MM-YYYY'),
                to_date('28-09-2021','DD-MM-YYYY')
                );
        INSERT INTO activite (id_joueur,debutactivite,dureeactivite) 
            VALUES ((SELECT id FROM joueur WHERE alias='Vidar389'),
                    to_date('05/06/2019','dd/mm/yyyy'),
                    18654);
        INSERT INTO activite (id_joueur,debutactivite,dureeactivite)
            VALUES ((SELECT id FROM joueur WHERE alias='Vidar389'),
                    to_date('04/07/2019','dd/mm/yyyy'),
                    22043);
        INSERT INTO activite (id_joueur,debutactivite,dureeactivite)
            VALUES ((SELECT id FROM joueur WHERE alias='Vidar389'),
                    to_date('19/09/2019','dd/mm/yyyy'),
                    15354);
        -- Temps passer par avatar et par monde de Vidar389
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'Vidar'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'Vidar389') AND debutActivite=to_date('05/06/2019','dd/mm/yyyy')), --DATE INVALIDE AVEC LES ACTIVITeS
                            18654);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'Fenrir'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'Vidar389')AND debutActivite=to_date('04/07/2019','dd/mm/yyyy')),
                            22043);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'Midgard'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'Vidar389')AND debutActivite=to_date('19/09/2019','dd/mm/yyyy')),
                            15354);
            
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'OrionX'),
                            (SELECT id FROM avatar WHERE nom='Vidar'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'Vidar389')AND debutActivite=to_date('05/06/2019','dd/mm/yyyy')),
                            18654,
                            to_date('19/09/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                                                (SELECT id FROM avatar WHERE nom='Vidar'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'Vidar389')AND debutActivite=to_date('19/09/2019','dd/mm/yyyy')),
                            22043,
                            to_date('04/09/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'FreeZo'),
                            (SELECT id FROM avatar WHERE nom='Vidar'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'Vidar389')AND debutActivite=to_date('04/07/2019','dd/mm/yyyy')),
                            15354
                            ,
                            to_date('04/09/2019','dd/mm/yyyy'));
            
            
    --ACTIVITeS HugoBourcier
        INSERT INTO activite(id_joueur, debutActivite,dureeActivite)
                VALUES (    (SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier'),
                            TO_DATE('19/05/2018','dd/mm/yyyy'),
                            108000);
                            
        INSERT INTO activite(id_joueur, debutActivite,dureeActivite)
                VALUES (    (SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier'),
                            TO_DATE('05/09/2018','dd/mm/yyyy'),
                            58321);
            
        INSERT INTO activite(id_joueur, debutActivite,dureeActivite)
                VALUES (    (SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier'),
                            TO_DATE('08/10/2018','dd/mm/yyyy'),
                            432566);
        
        -- Temps passer par avatar et par monde de HugoBourcier                    
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, debut_activite, duree)
                VALUES((SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            (SELECT id FROM activite WHERE debutActivite = TO_DATE('19/09/2019', 'dd/mm/yyyy') AND id_joueur=(SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier')),
                            TO_DATE('19/05/2018', 'dd/mm/yyyy'),
                            108000);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, debut_activite, duree)
                VALUES((SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            (SELECT id FROM activite WHERE debutActivite = TO_DATE('05/09/2019', 'dd/mm/yyyy')AND id_joueur=(SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier')),
                            TO_DATE('05/09/2018', 'dd/mm/yyyy'),
                            58321);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, debut_activite, duree)
                VALUES((SELECT id FROM avatar WHERE nom LIKE 'Moi'),
                            (SELECT id FROM activite WHERE debutActivite = TO_DATE('08/10/2019', 'dd/mm/yyyy')AND id_joueur=(SELECT id FROM joueur WHERE alias LIKE 'HugoBourcier')),
                            TO_DATE('08/10/2018', 'dd/mm/yyyy'),
                            432566);
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'FreeZo'),
                            (SELECT id FROM avatar WHERE nom='Moi'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'HugoBourcier')AND debutActivite=to_date('19/09/2019','dd/mm/yyyy')),
                            108000,
                            to_date('19/05/2018','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'DREAMX'),
                            (SELECT id FROM avatar WHERE nom='Moi'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'HugoBourcier')AND debutActivite=to_date('05/09/2019','dd/mm/yyyy')),
                            58321,
                            to_date('05/09/2018','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'OrionX'),
                            (SELECT id FROM avatar WHERE nom='Moi'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'HugoBourcier')AND debutActivite=to_date('08/10/2019','dd/mm/yyyy')),
                            432566,
                            to_date('08/10/2018','dd/mm/yyyy'));
                            
    --ACTIVITE petit-boursier1212
        INSERT INTO activite( id_joueur,debutActivite,dureeActivite)
            VALUES ((SELECT id FROM joueur WHERE alias='petit-boursier1212'), 
                    to_date('30/11/2017','dd/mm/yyyy'),
                    1200);
        INSERT INTO activite( id_joueur,debutActivite,dureeActivite)
            VALUES ((SELECT id FROM joueur WHERE alias='petit-boursier1212'),
                    to_date('03/12/2017','dd/mm/yyyy'),
                    6300);
        INSERT INTO activite( id_joueur,debutActivite,dureeActivite)
            VALUES ((SELECT id FROM joueur WHERE alias='petit-boursier1212'),
                    to_date('15/12/2017','dd/mm/yyyy'),
                    9250);
        -- Temps passer par avatar et par monde de petit-boursier1212                    
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, debut_activite, duree)
                VALUES(     (SELECT id FROM avatar WHERE nom LIKE 'mauvais_herbe'),
                            (SELECT id FROM activite WHERE debutActivite = TO_DATE('07/12/2019', 'dd/mm/yyyy')),
                            TO_DATE('30/11/2017', 'dd/mm/yyyy'),
                            1200);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, debut_activite, duree)
                VALUES(     (SELECT id FROM avatar WHERE nom LIKE 'mauvais_herbe'),
                            (SELECT id FROM activite WHERE debutActivite = TO_DATE('08/12/2019', 'dd/mm/yyyy')),
                            TO_DATE('03/12/2017', 'dd/mm/yyyy'),
                            6300);
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, debut_activite, duree)
                VALUES(     (SELECT id FROM avatar WHERE nom LIKE 'mauvais_herbe'),
                            (SELECT id FROM activite WHERE debutActivite = TO_DATE('09/12/2019', 'dd/mm/yyyy')),
                            TO_DATE('15/12/2017', 'dd/mm/yyyy'),
                            9250);
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'DREAMX'),
                            (SELECT id FROM avatar WHERE nom='mauvais_herbe'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'petit-boursier1212')AND debutActivite=to_date('19/09/2019','dd/mm/yyyy')),
                            1200,
                            to_date('30/11/2017','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'OrionX'),
                            (SELECT id FROM avatar WHERE nom='mauvais_herbe'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'petit-boursier1212')AND debutActivite=to_date('05/09/2019','dd/mm/yyyy')),
                            6300,
                            to_date('03/11/2017','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'FreeZo'),
                            (SELECT id FROM avatar WHERE nom='mauvais_herbe'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'petit-boursier1212')AND debutActivite=to_date('08/10/2019','dd/mm/yyyy')),
                            9250,
                            to_date('15/12/2017','dd/mm/yyyy'));
                
    --ACTIVITe MrPaul
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('05/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('06/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('07/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('08/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('09/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('10/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('11/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('12/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('13/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('14/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('15/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('16/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('17/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('18/06/2019','dd/mm/yyyy'),
                    3600);
        INSERT INTO activite(id_joueur,debutActivite,dureeActivite)
            VALUES((SELECT id FROM joueur WHERE alias = 'MrPaul'),
                    to_date('19/06/2019','dd/mm/yyyy'),
                    3600);
        -- Temps passer par avatar et par monde de paul
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('07/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('07/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('08/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('08/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('09/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('09/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('10/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('10/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('11/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('11/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('12/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('12/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('13/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('13/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('14/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('14/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul')AND debutActivite=to_date('15/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('15/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur= (SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('16/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('16/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'DoctorC++'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('17/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('17/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'DoctorC++'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('18/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('18/06/2019','dd/mm/yyyy'));
                            
            INSERT INTO temps_passe_avatar(id_avatar,id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM avatar WHERE nom = 'DoctorC++'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('19/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('19/06/2019','dd/mm/yyyy'));
                            
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'OrionX'),
                (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('07/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('07/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('08/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('08/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'DREAMX'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('09/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('09/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'FreeZo'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('10/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('10/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'FreeZo'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('11/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('11/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('12/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('12/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'DREAMX'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('13/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('13/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'OrionX'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('14/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('14/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('15/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('15/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite)   
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                            (SELECT id FROM avatar WHERE nom='TheSuitedMan*'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('16/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('16/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'FreeZo'),
                            (SELECT id FROM avatar WHERE nom='DoctorC++'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('17/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('17/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                            (SELECT id FROM avatar WHERE nom='DoctorC++'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('18/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('18/06/2019','dd/mm/yyyy'));
            INSERT INTO temps_passe_jeu(id_jeu, id_avatar, id_activite, duree, debut_activite) 
                VALUES(     (SELECT id FROM jeu WHERE sigle = 'deepho'),
                            (SELECT id FROM avatar WHERE nom='DoctorC++'),
                            (SELECT id FROM activite WHERE
                            id_joueur=(SELECT id FROM joueur WHERE alias = 'MrPaul') AND debutActivite=to_date('19/06/2019','dd/mm/yyyy')),
                            3600,
                            to_date('19/06/2019','dd/mm/yyyy'));
